//
//  NavigationController.swift
//  swift_lesson1_2
//
//  Created by Nazar Starantsov on 19.10.2019.
//  Copyright © 2019 16817252. All rights reserved.
//

import Foundation
import UIKit

class NavigationControllerDelegate: UIViewController, UINavigationControllerDelegate {
    func navigationController(_ navigationController: UINavigationController, willShow viewController: UIViewController, animated: Bool) {
        print(NSStringFromClass(type(of: viewController)))
    }
}
